.PHONY: run .build

run:
	python3 -m http.server

build:
	mkdir -p .public
	cp -r index.html css/ js/ .public/
	mv .public public
