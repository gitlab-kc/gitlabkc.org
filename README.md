# gitlabkc.org

Homepage for the GitLab KC community

## Development

- `make run` - Run the site with Python
- `make build` - Build the site into the public/ folder for GitLab Pages
